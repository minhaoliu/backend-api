<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\HTTP\Request;

class EventsController extends Controller
{
    public function newEvent(Request $request){
        $events = new Event();
        $events->date = $request->date;
        $events->time = $request->time;
        $events->eventname = $request->eventname;
        $events->eventplace = $request->eventplace;
        $events->description = $request->description;
        $events->save();
    }

    public function getEvent(Request $request){
        return Event::find($request->id);
    }

    public function getAllEvents(Request $request){
        return Event::all();
    }

    public function updateEvent(Request $request){
        $events = Event::find($request ->id);
        $events->date = $request->date;
        $events->time = $request->time;
        $events->eventname = $request->eventname;
        $events->eventplace = $request->eventplace;
        $events->description = $request->description;

        $events->save();
     }

     public function delete(Request $request){
        Event::findorFail($request->id)->delete();
        return ["message"=>"deleted"];
    }


}
