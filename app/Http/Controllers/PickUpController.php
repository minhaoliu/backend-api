<?php

namespace App\Http\Controllers;

use App\Pickup;
use Illuminate\HTTP\Request;

class PickUpController extends Controller
{
    public function newPickup(Request $request){
        $pickups = new Pickup();
        $pickups->firstname = $request->firstname;
        $pickups->lastname = $request->lastname;
        $pickups->email = $request->email;
        $pickups->phonenumber = $request->phonenumber;
        $pickups->arrivaldate = $request->arrivaldate;
        $pickups->arrivaltime = $request->arrivaltime;
        $pickups->arrivalairport = $request->arrivalairport;
        $pickups->flightnumber = $request->flightnumber;
        $pickups->nationality = $request->nationality;
        $pickups->message = $request->message;
        $pickups->save();
    }

    public function getPickup(Request $request){
        return Pickup::find($request->id);
    }

    public function getAllPickups(Request $request){
        return Pickup::all();
    }

    public function updatePickup(Request $request){
        $pickups = Pickup::find($request ->id);
        $pickups->firstname = $request->firstname;
        $pickups->lastname = $request->lastname;
        $pickups->email = $request->email;
        $pickups->phonenumber = $request->phonenumber;
        $pickups->arrivaldate = $request->arrivaldate;
        $pickups->arrivaltime = $request->arrivaltime;
        $pickups->arrivalairport = $request->arrivalairport;
        $pickups->flightnumber = $request->flightnumber;
        $pickups->nationality = $request->nationality;
        $pickups->message = $request->message;
        $pickups->save();
     }

     public function delete(Request $request){
        Pickup::findorFail($request->id)->delete();
        return ["message"=>"deleted"];
    }


}
