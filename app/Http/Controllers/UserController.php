<?php

namespace App\Http\Controllers;

use App\Mentor;
use App\User;
use Illuminate\HTTP\Request;

class UserController extends Controller
{
    public function makeUser(Request $request){
        $user = new User();
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->studentid = $request->studentid;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->country = $request->country;
        $user->department = $request->department;
        $user->save();
    }

    public function getUser(Request $request){
        return User::find($request->id);
    }

    public function getAllUsers(Request $request){
        return User::all();
    }

    public function updateUser(Request $request){
        $user = User::find($request ->id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->studentid = $request->studentid;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->country = $request->country;
        $user->department = $request->department;

        $user->save();
     }

     //search funtion for user by student id
    public function searchUser(Request $request) {
        $user = User::where("student_id", $request->ask)->orWhere("first_name", $request->ask)->orWhere("last_name", $request->ask)->get();
        return $user;
    }

    public function getMentor(Request $request) {
        $user = User::with('mentor')->find($request->id);
        return $user;
    }

    public function deleteUser(Request $request){
        User::findorFail($request->id)->delete();
        return ["message"=>"deleted"];
    }

    public function logincheck(Request $request)
    {
        return (User::where('email', $request->email)->where('password',$request->password)->first())?"true":"false";
    }

    public function getStudentsAndMentors(){
        return Mentor::with('students')->get();
    }

}
