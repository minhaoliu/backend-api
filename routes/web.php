<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//users
$router->post('/user/new', 'UserController@makeUser');
$router->get('/user/{id}', 'UserController@getUser');
$router->get('/users', 'UserController@getAllUsers');
$router->get('/user/delete/{id}', 'UserController@deleteUser');
$router->post('login', 'UserController@logincheck');

$router->patch('/user/edit/{id}' , 'UserController@updateUser');
$router->get('/user/mentor/{id}' , 'UserController@getMentor');

//events
$router->post('/event/new', 'EventsController@newEvent');
$router->get('/events', 'EventsController@getAllEvents');
$router->get('/event/delete/{id}', 'EventsController@delete');
$router->patch('/event/edit/{id}', 'EventsController@updateEvent');

//pickups
$router->post('/pickup/new', 'PickUpController@newPickup');
$router->get('/pickups', 'PickUpController@getAllPickups');
$router->get('/pickup/delete/{id}', 'PickUpController@delete');
$router->patch('/pickup/edit/{id}', 'PickUpController@updatePickup');

$router->get('/mentorsAndStudents', 'UserController@getStudentsAndMentors');

